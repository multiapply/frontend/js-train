// const intervalID = setInterval(myCallback, 1000, 'Interval-Run');

// function myCallback(msg) {
//   console.log(msg, Date.now());
// }

function stopChange() {
  clearInterval(colorIntervalID);
}

document.getElementById('stop').addEventListener('click', stopChange);

let colorIntervalID;

function startChange() {
  if(!colorIntervalID) {
    colorIntervalID = setInterval(changeRandomColor, 1000);
  }
}
// function changeColor() {
//   if (document.body.style.backgroundColor !== 'black'){
//     document.body.style.backgroundColor = 'black';
//     document.body.style.color = 'white';
//   } else {
//     document.body.style.backgroundColor = 'white';
//     document.body.style.color = 'black';
//   }
// }

function changeRandomColor() {
  const randomColor = Math.floor(Math.random() * 16777215).toString(16);
  document.body.style.backgroundColor = `#${randomColor}`;
}

document.getElementById('start').addEventListener('click', startChange);
