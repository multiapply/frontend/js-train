// Sample 1
function insideTimeout() {
  console.log('Hello from callback-1')
}
setTimeout(insideTimeout, 2000);

console.log('Hello from global scope')

// Sample 2
function changeText() {
  document.querySelector('h1').textContent = 'From Callback'
  console.log('Hello from callback-2')
}
// setTimeout(changeText, 2000);
const timerId = setTimeout(changeText, 3000);

function removeTimeOutHandler() {
  console.log(timerId)
  clearTimeout(timerId);
  console.log('Timer Cancelled')
}

document.querySelector('#cancel').addEventListener('click', removeTimeOutHandler)

